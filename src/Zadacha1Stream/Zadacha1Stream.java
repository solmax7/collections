package Zadacha1Stream;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;

public class Zadacha1Stream {

    public static void main(String[] args) {

       // Подсчитать количество повторений для каждого слова - (принять, что знаки пробела, знаки препинания являются разделителями слов).
       // Рекомендуется использовать Map&lt;String, Long&gt;.

        String str = "Подсчитать Подсчитать количество  для, повторений для каждого слова - (принять, что знаки пробела, знаки препинания являются разделителями слов). слова Рекомендуется использовать Map&lt;String, Long&gt;";

        Map<String,Long> map = new HashMap<>();

       map = Arrays.asList(str.split("[^а-яА-Яa-zA-Z]+")).stream().collect(Collectors.groupingBy(Function.identity(), counting()));



        for (Map.Entry entry : map.entrySet()) {
            System.out.println("Key: " + entry.getKey() + " Value: "
                    + entry.getValue());
        }

    }


}
