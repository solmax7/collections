package Zadacha1;

import sun.security.x509.FreshestCRLExtension;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Zadacha1 {

    public static void main(String[] args) {

       // Подсчитать количество повторений для каждого слова - (принять, что знаки пробела, знаки препинания являются разделителями слов).
       // Рекомендуется использовать Map&lt;String, Long&gt;.

        String str = "Подсчитать Подсчитать количество  для, повторений для каждого слова - (принять, что знаки пробела, знаки препинания являются разделителями слов). слова Рекомендуется использовать Map&lt;String, Long&gt;";

        Map<String,Long> map = new HashMap<>();

        Pattern pattern = Pattern.compile("(^[А-Я]|[а-я]|[А-Я])+");

        Matcher matcher = pattern.matcher(str);

        while (matcher.find()) {

            String s = str.substring(matcher.start(), matcher.end());
                Long frequency = map.get(s);

                map.put(s, frequency == null ? 1 : frequency + 1);

            //System.out.println(str.substring(matcher.start(), matcher.end()));
        }

        for (Map.Entry entry : map.entrySet()) {
            System.out.println("Key: " + entry.getKey() + " Value: "
                    + entry.getValue());
        }

    }

}
