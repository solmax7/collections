package Zadacha3;
import java.util.Comparator;

public class NewComparator {


        Comparator<String> ALPHABETICAL_ORDER = (str1, str2) -> {
            int res = String.CASE_INSENSITIVE_ORDER.compare(str1, str2);
            return (res != 0) ? res : str1.compareTo(str2);
        };


}
