package Zadacha3;

import java.util.*;

public class Zadacha3 {




    public static Set<String>HashWords(String s){

        return new HashSet<String>(Arrays.asList(s.split("[^а-яА-Яa-zA-Z]+")));

    }

    public static void main(String[] args) {

        String s = "Если кратко и лаконично, — великолепный инструмент от компании Pivotel для сохранения драгоценного времени в процессе создания приложения, исключающий потребность прямого подключения сторонних библиотек, написания внушительного ";

        List<String> arrays = new ArrayList<>(HashWords(s));

        NewComparator comparator = new NewComparator();

        arrays.sort(comparator.ALPHABETICAL_ORDER);

        System.out.println(arrays.toString());

    }
}

